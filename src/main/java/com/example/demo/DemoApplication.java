package com.example.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.util.StringUtils.isEmpty;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}

@RestController
@RequestMapping("/mobitv/blackout/224")
class FoxRestController {

    private static final Logger log = LoggerFactory.getLogger(FoxRestController.class);

    private final ObjectMapper mapper;

    FoxRestController(ObjectMapper mapper) {
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @RequestMapping(value = "/{provider}/**", method = RequestMethod.GET, consumes = "application/xml")
    public ResponseEntity<Void> getMethod(@PathVariable String provider, HttpServletRequest request, HttpEntity<String> entity) throws JsonProcessingException {
        logRequest(provider, RequestMethod.GET, entity.getHeaders(), request, entity.getBody());
        return ResponseEntity.status(200).build();
    }

    @RequestMapping(value = "/{provider}/**", method = RequestMethod.PUT, consumes = "application/xml")
    public ResponseEntity<Void> putMethod(@PathVariable String provider, HttpServletRequest request, HttpEntity<String> entity) throws JsonProcessingException {
        logRequest(provider, RequestMethod.PUT, entity.getHeaders(), request, entity.getBody());
        return ResponseEntity.status(201).build();
    }

    @RequestMapping(value = "/{provider}/**", method = RequestMethod.DELETE, consumes = "application/xml")
    public ResponseEntity<Void> deleteMethod(@PathVariable String provider, HttpServletRequest request, HttpEntity<String> entity) throws JsonProcessingException {
        logRequest(provider, RequestMethod.DELETE, entity.getHeaders(), request, entity.getBody());
        return ResponseEntity.status(204).build();
    }

    private void logRequest(String provider, RequestMethod method, HttpHeaders headers, HttpServletRequest request, String body) throws JsonProcessingException {
        log.info("-------------------------------------------------------------");
        log.info("Provider: " + provider);
        log.info("Request type: " + method.name());
        log.info("Request URI: " + request.getRequestURI() + "?" + request.getQueryString());
        log.info("Headers:");
        log.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(headers));
        if (!isEmpty(body)) {
            log.info("Request body:");
            log.info(body);
        }
        log.info("-------------------------------------------------------------\n\n\n");
    }
}